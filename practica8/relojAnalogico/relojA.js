
function relojAnalogico() {
  
  var horario = new Date(); 
 
  var horas = horario.getHours();
  
  var minutos = horario.getMinutes();
  
  var segundos = horario.getSeconds();
  

  var segundero = document.getElementById("segundos");
  var minutero = document.getElementById("minutos");
  var hor = document.getElementById("horas");
 
  var rotateS =  "rotate("+segundos * 6+"deg)";
  var rotateM =  "rotate("+minutos * 6+"deg)";
  var rotateH =  "rotate("+((horas * 30) + (minutos / 2))+"deg)";

//manillas sacadas de WebKit-Specific Style:-webkit-appearance

  segundero.style.webkitTransform = rotateS;
  minutero.style.webkitTransform = rotateM;  
  hor.style.webkitTransform = rotateH;
}

setInterval(function() {relojAnalogico()}, 1000);